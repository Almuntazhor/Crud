<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses'  =>  'HomeController@index',
    'as'    =>  'home'
]);
Route::get('student', [
    'as' => 'student', 'uses' => 'StudentController@index'
]);
Route::get('student/add', [
    'as' => 'student.add', 'uses' => 'StudentController@create'
]);

Route::post('student/add', [
    'as' => 'student.post', 'uses' => 'StudentController@store'
]);
Route::get('student/edit/{id}', [
    'as' => 'student.edit', 'uses' => 'StudentController@edit'
]);

Route::patch('student/edit/{id}', [
    'as' => 'student.patch', 'uses' => 'StudentController@update'
]);

Route::get('student/delete/{id}', [
    'as' => 'student.delete', 'uses' => 'StudentController@delete'
]);

Route::delete('student/delete/{id}', [
    'as' => 'student.destroy', 'uses' => 'StudentController@destroy'
]);