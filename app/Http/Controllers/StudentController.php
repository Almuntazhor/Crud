<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Students;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index() 
    {

        $students = Students::paginate(5);
        return view('students.index')->with('students', $students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'nis' => 'required',
            'nama' => 'required',
            'alamat' => 'required'
        ]);

        Students::create([
            'nis' => $request->input('nis'),
            'nama' => $request->input('nama'),
            'alamat' => $request->input('alamat')
        ]);

        return redirect()
            ->route('student')
            ->with('info', 'Data berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Students::findOrFail($id);

        return view('students.edit')->with('students', $students);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $students = Students::findOrFail($id);
    $this->validate($request, [
        'nis'    => 'required',
        'nama'   => 'required',
        'alamat' => 'required'
    ]);

    $input = $request->all();

    $students->fill($input)->save();

    return redirect()
        ->route('student')
        ->with('info', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {

        $students = Students::findOrFail($id);

        return view('students.delete')->with('students', $students);
    }

    public function destroy($id)
    {
        
        $students = Students::findOrFail($id);

        $students->destroy($id);

        return redirect()
            ->route('student')
            ->with('info', 'Data berhasil dihapus');
    }
}
