@extends('templates.default')
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Form Hapus Data Siswa</div>
                <div class="panel-body">
                    <form action=" {{ route('student.destroy', $students->id) }} " method="POST" class="form-horizontal">
                        <input type="hidden" name="_method" value="DELETE">
                        <div class="form-group">
                            <label for="" class="control-label col-md-3">Nis</label>
                            <div class="col-md-6">
                                <input type="text" name="nis" class="form-control" value="{{ Request::old('nis') ?: $students->nis }}" readonly="">
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-md-3">Nama</label>
                            <div class="col-md-6">
                                <input type="text" name="nama" class="form-control" value="{{ Request::old('nama') ?: $students->nama }}" readonly="">
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label col-md-3">Alamat</label>
                            <div class="col-md-6">
                                <input type="text" name="alamat" class="form-control" value="{{ Request::old('alamat') ?: $students->alamat }}" readonly="">
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-sm">Hapus</button>
                            <a href=" {{ route('student') }} " class="btn btn-primary btn-sm">Batal</a>
                        </div>
                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop